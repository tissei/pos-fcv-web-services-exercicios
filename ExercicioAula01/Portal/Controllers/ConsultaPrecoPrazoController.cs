﻿using System.Web.Mvc;
using Portal.Correios.CalculoPrecoPrazo;
using Portal.Models;

namespace Portal.Controllers
{
    public class ConsultaPrecoPrazoController : Controller
    {
        private CalcPrecoPrazoWSSoapClient client;

        public ConsultaPrecoPrazoController()
        {
            client = new CalcPrecoPrazoWSSoapClient();
        }

        public ActionResult Index()
        {
            return View(new ConsultaPrecoPrazo());
        }

        public ActionResult Resultado(ConsultaPrecoPrazo consulta)
        {
            var resposta = client.CalcPrecoPrazo("", "", consulta.CdServico, consulta.CepOrigem, consulta.CepDestino,
                consulta.VlPeso, consulta.CdFormato, consulta.VlComprimento, consulta.VlAltura, consulta.VlLargura,
                consulta.VlDiametro, consulta.CdMaoPropria, consulta.VlValorDeclarado, consulta.CdAvisoRecebimento);

            return View(resposta.Servicos);
        }
    }
}
