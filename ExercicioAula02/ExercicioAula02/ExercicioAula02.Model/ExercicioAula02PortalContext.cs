﻿using System.Data.Entity;

namespace ExercicioAula02.Model
{
    public class ExercicioAula02PortalContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public ExercicioAula02PortalContext() : base("name=ExercicioAula02PortalContext")
        {
        }

        public System.Data.Entity.DbSet<ExercicioAula02.Model.Pokemon> Pokemons { get; set; }
    }
}
