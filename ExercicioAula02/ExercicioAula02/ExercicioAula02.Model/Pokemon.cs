﻿namespace ExercicioAula02.Model
{
    public class Pokemon
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Type Type { get; set; }
        public string Image { get; set; }
    }
}
