﻿using System.Net;
using System.Web.Mvc;
using ExercicioAula02.Model;
using ExercicioAula02.Portal.Services;

namespace ExercicioAula02.Portal.Controllers
{
    public class PokemonsController : Controller
    { 
        private readonly PokemonServiceClient pokemonsService;

        public PokemonsController(PokemonServiceClient pokemonsService)
        {
            this.pokemonsService = pokemonsService;
        }

        // GET: Pokemons
        public ActionResult Index()
        {
            return View(pokemonsService.Listar());
        }

        // GET: Pokemons/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pokemon pokemon = pokemonsService.Consultar(id.Value);
            if (pokemon == null)
            {
                return HttpNotFound();
            }
            return View(pokemon);
        }

        // GET: Pokemons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pokemons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Type,Image")] Pokemon pokemon)
        {
            if (ModelState.IsValid)
            {
                pokemonsService.Cadastrar(pokemon);
                return RedirectToAction("Index");
            }

            return View(pokemon);
        }

        // GET: Pokemons/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pokemon pokemon = pokemonsService.Consultar(id.Value);
            if (pokemon == null)
            {
                return HttpNotFound();
            }
            return View(pokemon);
        }

        // POST: Pokemons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Type,Image")] Pokemon pokemon)
        {
            if (ModelState.IsValid)
            {
                pokemonsService.Editar(pokemon);
                return RedirectToAction("Index");
            }
            return View(pokemon);
        }

        // GET: Pokemons/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pokemon pokemon = pokemonsService.Consultar(id.Value);
            if (pokemon == null)
            {
                return HttpNotFound();
            }
            return View(pokemon);
        }

        // POST: Pokemons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            var pokemon = pokemonsService.Consultar(id);
            pokemonsService.Excluir(pokemon);
            return RedirectToAction("Index");
        }
    }
}
