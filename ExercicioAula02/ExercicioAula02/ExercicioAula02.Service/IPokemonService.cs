﻿using ExercicioAula02.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExercicioAula02.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPokemonService" in both code and config file together.
    [ServiceContract]
    public interface IPokemonService
    {
        [OperationContract]
        List<Pokemon> Listar();

        [OperationContract]
        Pokemon Consultar(long idPokemon);

        [OperationContract]
        void Cadastrar(Pokemon pokemon);

        [OperationContract]
        void Editar(Pokemon pokemon);

        [OperationContract]
        void Excluir(long idPokemon);
    }
}
