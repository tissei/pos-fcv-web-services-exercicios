﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ExercicioAula02.Model;

namespace ExercicioAula02.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PokemonService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PokemonService.svc or PokemonService.svc.cs at the Solution Explorer and start debugging.
    public class PokemonService : IPokemonService
    {
        static List<Pokemon> pokemons;

        public PokemonService()
        {
            pokemons = new List<Pokemon>();

            pokemons.Add(new Pokemon()
            {
                Id = 1,
                Image = "MulaManca",
                Name = "PokeZé",
                Type = Model.Type.Grass
            });

            pokemons.Add(new Pokemon()
            {
                Id = 2,
                Image = "Teste",
                Name = "PokeJeferson",
                Type = Model.Type.Fire
            });

            pokemons.Add(new Pokemon()
            {
                Id = 2,
                Image = "Conz",
                Name = "PokeConz",
                Type = Model.Type.Grass
            });
        }

        public void Cadastrar(Pokemon pokemon)
        {
            pokemons.Add(pokemon);
        }

        public Pokemon Consultar(long idPokemon)
        {
            return pokemons.Find(e => e.Id == idPokemon);
        }

        public void Editar(Pokemon pokemon)
        {
            throw new NotImplementedException();
        }

        public void Excluir(long idPokemon)
        {
            var pokemon = pokemons.Find(e => e.Id == idPokemon);
            pokemons.Remove(pokemon);
        }

        public List<Pokemon> Listar()
        {
            return pokemons;
        }
    }
}
